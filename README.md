# monapi

## update api

```bash
go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@v1.12.4
oapi-codegen -package api monapi-oapi.yaml > api/monapi.gen.go
```

## build

### local go build and run with go (not prod)

```bash
go run .
```

### local docker build (eq prod)

```bash
docker build -t monapi:local .
docker run -it --rm \
  --name monapi \
  --mount type=bind,source="$(pwd)"/example/config.yaml,target=/config.yaml \
  -p 8080:8080 \
  -p 9090:9090 \
  monapi:local
```

### cross build (eq release)

```bash
docker run --rm -v "$PWD":/usr/src/monapi -w /usr/src/monapi -e GOOS=linux -e GOARCH=amd64 golang:1.20 go build -v -buildvcs=false -o monapi
docker run --rm -v "$PWD":/usr/src/monapi -w /usr/src/monapi -e GOOS=linux -e GOARCH=arm -e GOARM=5 golang:1.20 go build -v -buildvcs=false -o monapi
docker run --rm -v "$PWD":/usr/src/monapi -w /usr/src/monapi -e GOOS=windows -e GOARCH=amd64 golang:1.20 go build -v -buildvcs=false -o monapi.exe
```

## exec docker image from ghcr.io (prod)

```bash
docker pull ghcr.io/sylvainleorg/monapi:main
docker run -it --rm --name monapi \
  -p 1323:1323 \
  -p 9360:9360 \
  ghcr.io/sylvainleorg/monapi:main
docker run -it --rm --name monapi \
  -p 1323:1323 \
  -p 9360:9360 \
  --mount type=bind,source="$(pwd)"/example/monapi.yaml,target=/monapi.yaml \
  ghcr.io/sylvainleorg/monapi:main
```
